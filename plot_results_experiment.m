gfdm = [-30	810	0;
    -25	810	0;
    -20	810	0;
    -17	809	1;
    -15	810	0;
    -14	804	18;
    -13	775	105;
    -12	776	95;
    -11	671	461;
    -10	650	554;
    -9	680	850;
    -8	548	971;
    -7	441	1464];
scfdma = [-30	810	0;
    -25	810	0;
    -20	800	27;
    -18	799	32;
    -17	792	52;
    -16	710	289;
    -15	647	478;
    -14	604	616;
    -13	480	1063;
    -12	452	1159;
    -11	318	1648;
    -10	190	2389;
    -9	137	2812];
ofdm = [-30	810	0;
    -25	810	0;
    -20	809	0;
    -15	779	84;
    -14	713	261;
    -13	520	877;
    -12	477	1034;
    -11	422	1223;
    -10	472	1157;
    -9	367	1504];

%%
set(0, 'DefaultLineMarkerSize', 10);
set(0, 'Defaultaxesfontsize', 14);
set(0, 'DefaultLineLineWidth', 2.5);

offset = 22-4.8;

h=figure(1);
hold off
plot(gfdm(:,1)+offset,smooth(gfdm(:,2)),'b');
hold on
plot(ofdm(:,1)+offset,smooth(ofdm(:,2)),'k--');
plot(scfdma(:,1)+offset,smooth(scfdma(:,2)),'r-.');
legend('GFDM','OFDM','SC-FDMA','Location','SouthWest');
xlabel('ISR [dB]')
ylabel('L3 Goodput [kbps]')
xlim([-10 10])
ylim([0 1000])
grid on
saveas(h,'experimental_results.eps','epsc2')
