att = [20 15 13 11 10 9 8 7 6 5 4 3];
gfdm1 = [809	810	807	810	808	804	797	786	783	731	658];
gfdm2 = [809	810	807	810	810	808	794	799	680	697	613	397];
ofdm = [803	807	770	734	716	727	331];

Ptx_gfdm1 = -17.5-att;
Ptx_gfdm2 = -18-att;
Ptx_ofdm = -17.5-att;

set(0, 'DefaultLineMarkerSize', 10);
set(0, 'Defaultaxesfontsize', 14);
set(0, 'DefaultLineLineWidth', 2.5);

figure(1)
hold off
%plot(Ptx_gfdm1(1:length(gfdm1)),smooth(gfdm1),'r');
plot(Ptx_gfdm2(1:length(gfdm2)),smooth(gfdm2),'b-');
hold on
plot(Ptx_ofdm(1:length(ofdm)),smooth(ofdm),'k--');
legend('GFDM','OFDM','Location','SouthWest');
xlabel('Tx Power secondary [dBm]')
ylabel('L3 Goodput [kbps]')
grid on
