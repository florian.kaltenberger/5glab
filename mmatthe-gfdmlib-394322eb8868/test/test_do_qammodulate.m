function test_suite = test_do_qammodulate
    initTestSuite;

function test_CorrectNorm_QPSK
    s = [0:3]';
    d = do_qammodulate(s, 2);

    assertElementsAlmostEqual(mean(abs(d).^2), 1);

function test_CorrectNorm_64QAM
    s = [0:63]';
    d = do_qammodulate(s, 6);

    assertElementsAlmostEqual(mean(abs(d).^2), 1);