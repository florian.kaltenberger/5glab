function test_suite = test_do_unmap
    initTestSuite;

function test_fullSet
    p = get_defaultGFDM('small');
    p.M = 2;
    p.K = 3; p.Kon = 3;

    D = [1 2; 3 4; 5 6];
    assertEqual([1 3 5 2 4 6]', do_unmap(p, D));

function test_kset
    p = get_defaultGFDM('small');
    p.M = 2;
    p.K = 3; p.Kset = [0 2];

    D = [1 3; 7 7; 2 4];
    assertEqual([1 2 3 4]', do_unmap(p, D));