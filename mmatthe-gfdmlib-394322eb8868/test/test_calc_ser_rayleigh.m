function test_suite = test_calc_ser_rayleigh
    initTestSuite;


function test_flatChannel_ZF
    p = get_defaultGFDM('BER');

    assertElementsAlmostEqual(0.07763815, calc_ser_rayleigh(p, 'ZF', 11, 1));
    assertElementsAlmostEqual([0.13694486, 0.07763815], calc_ser_rayleigh(p, 'ZF', [8, 11], 1));
