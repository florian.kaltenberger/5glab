function test_suite = test_do_map
    initTestSuite;

function test_fullMap
    p = get_defaultGFDM('small');
    p.M = 2; p.K = 3; p.Kon = 3;

    s = 1:6;
    D = do_map(p, s);
    assertEqual([1 4; 2 5; 3 6], D);

function test_kset
    p = get_defaultGFDM('small');
    p.M = 2;
    p.K = 3; p.Kon = 3; p.Kset = [0 2];

    s = [1:4]';
    D = do_map(p, s);

    assertEqual([1 3; 0 0; 2 4], D);

function test_mset
     p = get_defaultGFDM('small');
     p.M = 3; p.Mset = [0 2];
     p.K = 2; p.Kon = 2;

     s = [1:4]';
     D = do_map(p, s);

     assertEqual([1 0 3; 2 0 4], D);