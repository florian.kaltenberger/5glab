function test_suite = test_do_demodulate
    initTestSuite;

function test_small_MF
    p = get_defaultGFDM('small');
    p.pulse = 'dirichlet';

    D = do_map(p, get_random_symbols(p));
    x = do_modulate(p, D);

    Dh = do_demodulate(p, x, 'MF');
    assertElementsAlmostEqual(D, Dh);

function test_BER_ZF
    p = get_defaultGFDM('BER');

    D = do_map(p, get_random_symbols(p));
    x = do_modulate(p, D);

    Dh = do_demodulate(p, x, 'ZF');

    assertElementsAlmostEqual(D, Dh, 'absolute', 0.05);

function test_oQAM
    p = get_defaultGFDM('oQAM');

    D = do_map(p, get_random_symbols(p));
    x = do_modulate(p, D);

    Dh = do_demodulate(p, x);
    assertElementsAlmostEqual(D, Dh, 'absolute', 0.05);

function test_oQAM_td
    p = get_defaultGFDM('oQAM');
    p.pulse = 'rrc_td';

    D = do_map(p, get_random_symbols(p));
    x = do_modulate(p, D);

    Dh = do_demodulate(p, x);
    assertElementsAlmostEqual(D, Dh, 'absolute', 0.05);
