function test_suite = test_get_transmitter_matrix
    initTestSuite;

function test_correctSize
    p = get_defaultGFDM('small');

    A = get_transmitter_matrix(p);

    assertEqual(size(A), [p.M*p.K, p.M*p.K]);

function test_Columns_correct
    p = get_defaultGFDM('small');
    g00 = get_transmitter_pulse(p);
    g10 = g00 .* exp(2j*pi/p.K*[0:p.K*p.M-1]');
    g01 = circshift(g00, p.K);

    A = get_transmitter_matrix(p);

    assertElementsAlmostEqual(A(:,1), g00);
    assertElementsAlmostEqual(A(:,2), g10);
    assertElementsAlmostEqual(A(:,p.K+1), g01);
