function test_suite = test_do_channel
    initTestSuite;

function test_NoNoise_Identity
    x = [0:1000]';

    xch = do_channel(x, 1, 99999);
    assertElementsAlmostEqual(x, xch);

function test_NoNoiseImp_AppliesImp
    x = [0:1000]';

    xch = do_channel(x, -1j, 99999);
    assertElementsAlmostEqual(-1j*x, xch);

function test_Noise_correctVariance
    x = [0:10000]';

    xch = do_channel(x, 1, 5);
    assertElementsAlmostEqual(var(x-xch), 10^(-5/10), 'absolute', 0.05);