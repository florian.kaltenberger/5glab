function test_suite = test_get_random_symbols
    initTestSuite;

function test_correctLength
    p = get_defaultGFDM('BER');

    s = get_random_symbols(p);
    assertEqual(size(s), [p.M*p.K, 1]);

function test_correctValues
    p = get_defaultGFDM('BER');
    p.mu = 4; % 16-QAM

    s = get_random_symbols(p);
    assertEqual([min(s), max(s)], [0, 4^2-1]);
