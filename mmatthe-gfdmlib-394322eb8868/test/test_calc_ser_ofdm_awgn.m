function test_suite = test_calc_ser_ofdm_awgn
    initTestSuite;

function test_flatChannel_values
    % These values have been calculated from my python reference
    assertElementsAlmostEqual(0.634802483831, calc_ser_ofdm_awgn(4, 3));
    assertElementsAlmostEqual(0.387933e-3, calc_ser_ofdm_awgn(2, 11));

    assertElementsAlmostEqual([4.54849493e-2, 1.19727201e-2], calc_ser_ofdm_awgn(2, [6, 8]));

function test_FSC_values
    chan = [1 0 0 0 0.5];

    assertElementsAlmostEqual(0.10977275, calc_ser_ofdm_awgn(2, 5, chan, 16))
    assertElementsAlmostEqual([0.10977275, 0.06775905], calc_ser_ofdm_awgn(2, [5, 7], chan, 16))