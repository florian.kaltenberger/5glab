C = dirrec('.', '.m');
D = {};
for i = 1:length(C)
    clear(C{i}(3:end-2));
    D{i} = fileparts(C{i});
end
D = unique(D);

runtests(D{:});
