function test_suite = test_get_transmitter_pulse
    initTestSuite;

function test_createsRRC_correctShape
    p = get_defaultGFDM('BER');

    g = get_transmitter_pulse(p);
    assertEqual(size(g), [p.K*p.M, 1]);

function test_createsRC_hasCorrectShape
    p = get_defaultGFDM('BER');
    p.pulse = 'rc';

    assertEqual(size(get_transmitter_pulse(p)), [p.K*p.M, 1]);

function test_createsDirichlet_hasCorrectShape
    p = get_defaultGFDM('BER');
    p.pulse = 'dirichlet';

    assertEqual(size(get_transmitter_pulse(p)), [p.K*p.M, 1]);

function test_RCFD_hasCorrectSize
    p = get_defaultGFDM('BER');
    p.pulse = 'rc_fd';

    assertEqual(size(get_transmitter_pulse(p)), [p.K*p.M, 1]);

function test_RCTD_hasCorrectSize
    p = get_defaultGFDM('BER');
    p.pulse = 'rc_td';

    assertEqual(size(get_transmitter_pulse(p)), [p.K*p.M, 1]);