function test_suite = test_get_receiver_pulse
    initTestSuite;

function test_matchedFilter
    p = get_defaultGFDM('BER');
    g00 = get_transmitter_pulse(p);

    assertElementsAlmostEqual(flipud(conj(g00)), get_receiver_pulse(p, 'MF'));

function test_zeroForcing
    p = get_defaultGFDM('small');
    A = get_transmitter_matrix(p);
    Ai = pinv(A);

    gi = get_receiver_pulse(p, 'ZF');
    assertElementsAlmostEqual(Ai(1,:)', gi);
    
function test_MMSE_even
    p = get_defaultGFDM('small');
    p.M = 5;
    p.K = 4;
    p.sigmaN = 0.1;
    
    A = get_transmitter_matrix(p);
    B = (A'*A+p.sigmaN*eye(size(A)))\A';
    
    gi = get_receiver_pulse(p, 'MMSE');
    assertElementsAlmostEqual(B(1,:).', gi);

function test_MMSE_odd
    p = get_defaultGFDM('small');
    p.M = 4;
    p.K = 4;
    p.sigmaN = 0.1;
    
    A = get_transmitter_matrix(p);
    B = (A'*A+p.sigmaN*eye(size(A)))\A';
    
    gi = get_receiver_pulse(p, 'MMSE');
    assertElementsAlmostEqual(B(1,:).', gi);

