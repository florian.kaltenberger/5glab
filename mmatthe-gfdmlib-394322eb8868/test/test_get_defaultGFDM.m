function test_suite = test_get_defaultGFDM
    initTestSuite;

function test_defaultValues
    names = {'small', 'BER', 'TTI'};
    Ls = [];
    as = [];
    pulses = {};
    mus = [];
    for i=1:length(names)
        p = get_defaultGFDM(names{i});
        Ls = [Ls p.L];
        as = [as p.a];
        pulses{i} = p.pulse;
        mus = [mus p.mu];
    end

    assertTrue(all(Ls == 2));
    assertTrue(all(as == 0.5));
    assertTrue(all(mus == 2));
    assertTrue(all(cellfun(@(x) strcmp(x, 'rrc'), pulses)));

function test_small
    p = get_defaultGFDM('small');
    assertEqual([p.M, p.K, p.Kon], [3, 4, 4]);

function test_TTI
    p = get_defaultGFDM('TTI');
    assertEqual([p.M, p.K, p.Kon], [15, 2048, 1200]);

function test_BER
    p = get_defaultGFDM('BER');
    assertEqual([p.M, p.K, p.Kon], [5, 128, 128]);

function test_OFDM
    p = get_defaultGFDM('OFDM');
    assertEqual([p.M, p.K, p.Kon, p.a], [1, 2048, 1200, 0]);
    assertEqual(p.pulse, 'rc_td');