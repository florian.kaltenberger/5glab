function test_suite = test_get_noise_enhancement_factor
    initTestSuite;

function test_rrc
    p = get_defaultGFDM('BER');
    f = get_noise_enhancement_factor(p);

    assertElementsAlmostEqual(1.2416509, f);