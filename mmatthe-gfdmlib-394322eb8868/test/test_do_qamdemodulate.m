function test_suite = test_do_qamdemodulate
    initTestSuite;

function test_QPSK_demodulates
    s = [0:3]';
    d = do_qammodulate(s, 2);

    sh = do_qamdemodulate(d, 2);
    assertEqual(s, sh);

function test_64QAM_demodulate
    s = [0:63]';
    d = do_qammodulate(s, 6);

    sh = do_qamdemodulate(d, 6);
    assertEqual(s, sh);