function test_suite = test_rrc
    initTestSuite;

function test_isNormalized
    g = rrc(5, 16, 0.25);
    assertElementsAlmostEqual(sum(abs(g).^2), 1);