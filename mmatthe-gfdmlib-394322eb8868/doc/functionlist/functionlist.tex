% Created 2013-10-22 Di 09:34
\documentclass[11pt]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{xstring}
\tolerance=1000
\usepackage{datetime}
\usepackage{underscore}
\usepackage[framemethod=tikz]{mdframed}

\usepackage{tikz}
\usetikzlibrary{patterns}
\usetikzlibrary{arrows}
\usetikzlibrary{decorations.text}

\title{Unified MATLAB Library for GFDM\\ - Proposal -}
\author{Max}
\date{\today\, at \currenttime}

\newenvironment{remark}{\vspace{2mm}\begin{mdframed}[hidealllines=true,backgroundcolor=gray!20]\paragraph{Remark:}}{\end{mdframed}}

\newenvironment{function}[1]{%
\addcontentsline{toc}{subsection}{#1}
\subsubsection*{Function\hfill \code{#1}}
\vspace{-2mm}\hrule\vspace{2mm}}{}

\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\mat}[1]{\ensuremath{\mathbf{#1}}}

\setlength{\parindent}{0pt}

\begin{document}

\maketitle

\section{Remarks}
\label{sec-1}

This document proposes a MATLAB library for GFDM that can be used for
new projects and reference implementations.  Furthermore, new
colleagues and students can use it to easily understand the
implementation of GFDM and have a guided start into GFDM.

The document tries to conform with the notation proposed by Nicola.
Currently, only basic functions for GFDM transmission, signal
generation and naive reception are included, nothing fancy like
channel estimation/equalization, iterative receivers etc.  The main
goal of the library is to define a common code base for GFDM
simulations and to reduce the code redundancy among all the different
simulation scripts once common parts are extracted into distinct
functions.

The functions are grouped into several parts:

\begin{description}
\item[get_*] These functions return objects that are commonly used for
  GFDM simulations.
\item[calc_*] These functions calculate (not only) performance values
  based on the passed objects.
\item[do_*] These more complex functions do actual simulation steps.
\end{description}

\subsection*{Coding style}
Where possible, function and variable names are written
\code{with_underscore_between_words}.  Constants are written in
capital letters.

Each function should be accompanied by some unit tests.  The tests
should reside in the \code{tests} directory of the library.  The
Matlab unit testing framework is used
(\url{http://www.mathworks.de/de/help/matlab/write-unit-tests-1.html})
which appears to be easy to adapt.

\clearpage
\tableofcontents
\clearpage

\section{The GFDM parameter structure}
\label{sec-2}

The GFDM parameter structure (denoted by \code{p}) is a common
parameter to all library functions. It contains the configuration of the
GFDM system. Members:


\begin{center}
\begin{tabular}{lp{12cm}}
 Name                &  Description                                                    \\
\hline
 M                   &  number of subsymbols in each GFDM block/symbol                \\
 Mon                 &  number of active subsymbols      \\
 Mset                &  set of active subsymbols \\
 K                   &  number of subcarrier in each GFDM block/symbol                 \\
 Kon                 &  number of active subcarriers, index starting
 from zero.                                   \\
 Kset                &  set of active subcarriers, index starting from
 zero.\\
 N                   &  number of samples per block (DFT size, M*K)
 (\emph{needs to be discussed if this should be included})                    \\
 L                   &  upsampling factor for FFT transmitter/receiver
 or number of overlapping subcarriers.                \\
 pulse               &  name of the pulse shaping filter                               \\
 a                   &  rolloff factor of the pulse shaping filter                     \\
 mu                  &  modulation order (bits per symbol) 2 => QPSK, 4 => 16QAM etc.  \\
 NCP                 &  number of samples for the CP                                   \\
 % b                   &  rolloff factor for the CP window                               \\
 % CPwindow            &  name of the window function on the CP                          \\

\end{tabular}
\end{center}

Note that not all members are required by all library functions.

The switched-on subcarriers and subsymbols are determined by the
fields \code{Kon}/\code{Kset} and \code{Mon}/\code{Mset},
respectively. See the functions \code{get_mset} and
\code{get_kset} for more information.

\begin{function}{\code{p=get_defaultGFDM(type)}}
This function returns a simplified version of the GFDM parameter
structure (i.e. not all parameters are set and some are set to default
values) so that simple setups can be set up easily.  The parameter
\code{kind} names the GFDM configuration, i.e. the corresponding
values for \code{N, M, K} etc.  Planned configurations are:

\begin{center}
  \begin{tabular}{lllllll}
    Name & M & K & Kon \\    \hline
    \code{BER} & 5 & 128 & 128 \\
    \code{small} & 3 & 4 & 4\\
    \code{TTI} & 15 & 2048 & 1200
  \end{tabular}
\end{center}

\begin{remark}
The \code{BER} configuration relates to the paper ``Bit Error Rate of
Generalized Frequency Division Multiplexing'' \cite{Michailow2012e}.
\end{remark}
\vspace{2mm}

\begin{remark}
It would be great if these configurations could be
read from a CSV file instead of being hard-coded into the
.m-file.  This way, new configurations can be easily added without
having to modify the Matlab code.
\end{remark}

The following default values are set for all of these configurations:
\begin{center}
  \begin{tabular}{rrrrrrr}
    Name &  L & \code{pulse} & \code{a} & \code{mu} & \code{NCP} \\\hline
    Value & 2 & \code{rrc} & 0.5 & 2 & 0 \\
  \end{tabular}
\end{center}

Any other values of the parameter structure are not set and shall be
set manually when needed.
\end{function}

\section{Proposed processing chain}
The proposed transmitter and receiver processing for library is
presented in the following figure.  Functions provided by MATLAB are
shown in light gray.

\begin{tikzpicture}
\tikzstyle{mybox}=[rectangle,draw,minimum height=8mm,minimum width=15mm,thick,align=center]
\tikzstyle{mlbox}=[rectangle,draw,gray,minimum height=8mm,minimum width=15mm,thick,align=center]
\tikzstyle{mycircle}=[circle,draw,thick,align=center]

\begin{scope}
  \node at (0,  0) [mybox] (11) {
    \begin{tabular}{l}
      \code{get_random}\\\code{_symbols}
    \end{tabular}};

  \node at (4, -1) [mlbox] (16) {\code{gray2bin}};
  \node at (4,  0) [mlbox] (12) {\code{qammod}};
  \node at (6.5,0) [mybox] (13) {\code{do_map}};
  \node at (9.5, 0) [mybox] (14) {\code{do_modulate}};
  \node at (13, 0) [mycircle] (15) {\emph{Channel}};

  \draw [->,thick] (11.east) -- (12.west);
  \draw [->,thick] (2,0) -- (2,-1) -- (16.west);
  \draw [->,thick] (16.east) -- +(1,0);
  \draw [->,thick] (12.east) -- (13.west);
  \draw [->,thick] (13.east) -- (14.west);
  \draw [->,thick] (14.east) -- (15.west);

  % \draw[help lines] (0,-1) grid (16,1);
\end{scope}

\begin{scope}[yshift=0.0cm]
  \node [rotate=90,right] at (2.1,0) {\small$K_{on}M_{on}\times 1$};
  \node [rotate=90,right] at (5.2,0) {\small$K_{on}M_{on}\times 1$};
  \node [rotate=90,right] at (7.8,0) {\small$K\times M$};
  \node [rotate=90,right] at (11.3,0) {\small$KM\times 1$};
  \node [below] at (5.5, -1) {bits};
\end{scope}

\begin{scope}[yshift=-3.5cm,xshift=-0.5cm]
  \node at (0,0) [mycircle]  (15) {\emph{Channel}};
  \node at (3.5,0) [mybox] (14) {\code{do_demodulate}};
  \node at (7,0) [mybox] (13) {\code{do_demap}};
  \node at (10,0) [mlbox] (12) {\code{qamdemod}};
  \node at (13,0) [mybox] (11) {$\hat{d}$};
  \node at (13,-1) [mlbox] (16) {\code{gray2bin}};
  % \draw[help lines] (0,-1) grid (16,1);

  \draw [->,thick] (15.east) -- (14.west);
  \draw [->,thick] (14.east) -- (13.west);
  \draw [->,thick] (13.east) -- (12.west);
  \draw [->,thick] (12.east) -- (11.west);
  \draw [->,thick] (11.5,0) -- (11.5,-1) -- (16.west);
  \draw [->,thick] (16.east) -- +(1,0);
\end{scope}

\begin{scope}[yshift=-3.5cm,xshift=-.5cm]
  \node [rotate=90,right] at (1.3,0) {\small$KM\times1$};
  \node [rotate=90,right] at (5.5,0) {\small$K\times M$};
  \node [rotate=90,right] at (8.5,0) {\small$K_{on}M_{on}\times 1$};
  \node [rotate=90,right] at (11.3,0) {\small$K_{on}M_{on}\times 1$};
  \node [below] at (14.5, -1) {bits};

\end{scope}
\end{tikzpicture}

\subsection*{Transmitter}
To create a transmit time signal, the following steps are carried out
(without CP and pilot information):
\begin{enumerate}
\item \textbf{Create random data symbols}\\
The function \code{get_random_symbols} returns a sequence of integers
in range $0\ldots 2^\mu-1$ which are the transmitted data symbols.
The corresponding bit stream can be acquired by the Matlab function
\code{gray2bin}.
\item \textbf{QAM-Modulate the symbols to QAM symbols}\\
Map the integers in range $0\ldots 2^\mu-1$ to a quadratic QAM
modulation using the matlab function \code{qammod}.
\item \textbf{Map the symbol stream to the data matrix}\\
The one-dimensional symbol stream is mapped to the data matrix
\mat{D} according to the values of $K_{set}$ and $M_{set}$.  Empty
sub-carriers and sub-symbols in $\mat{D}$ are set to zero.

Afterwards there is the possibility to insert specific pilots or other
information in the slots where no data is present.
\item \textbf{GFDM-Modulate the data matrix}\\
The matrix $\mat{D}$ is processed with the GFDM modulation scheme to
produce a time-domain signal with length $KM$ that can be processed further.
\end{enumerate}


\subsection*{Receiver}
To process data coming from a channel the following steps are carried
out (ignores CP and synchronization/estimation/equalization).
Actually, the steps from the transmission are applied in inverse order:
\begin{enumerate}
\item \textbf{GFDM-demodulate to the data matrix}\\
The received and equalized and synchronized signal demodulated with
the GFDM demodulation scheme.  Different demodulator types (ZF, MF,
...) can be applied here.  The received data matrix $\hat{\mat{D}}$ is
created.
\item \textbf{Demap the data matrix to a symbol stream}\\
Reverse the mapping operation. Concatenate all values of
$\hat{\mat{D}}$ that correspond to data slots (according to $K_{set}$
and $M_{set}$) into a one-dimensional vector.
\item \textbf{QAM-demodulate the data stream}\\
Demap the received constellation symbols to integer values in the
range  $0\ldots 2^\mu-1$.
\item \textbf{Process received symbols}\\
The received symbols are now represented as integers in the range
$0\ldots 2^\mu-1$ again. To acquire the corresponding bit stream use
the Matlab function \code{gray2bin}.
\end{enumerate}


\section{\code{Get} Functions}
\begin{function}{A = get_transmitter_matrix(p)}
Calculate the transmitter matrix \code{A} according to the algorithm
in \cite{Michailow2012e}. No CP extension is added to the matrix.

It creates a full square matrix with $MK\times{}MK$ elements,
independent of the values of \code{Mon} and \code{Kon}.
\end{function}

\begin{function}{B = get_receiver_matrix(p, recType, snr)}
Calculate the receiver matrix according to \cite{Michailow2012e}.
The value of \code{recType} describes the receiver type:

\begin{center}
  \begin{tabular}{ll}
    Value & Type \\\hline
    \code{MF} & Matched filter matrix $\mat{A}^H$\\
    \code{ZF} & Zero-forcing matrix $\mat{A}^+$\\
    \code{MMSE} & Minimum mean square error receiver matrix
  \end{tabular}
\end{center}

The matrix is square independent of the values of \code{Kon} and
\code{Mon}.
\end{function}

\begin{function}{Q = get\_offset\_matrix(p, dF, dT)}
  Return a matrix that, when multiplied with a time domain signal,
  circularly shifts it in time and frequency by the amounts given as
  the parameters.  The values are relative to one subcarrier and
  subsymbol, respectively.
\end{function}

\begin{function}{[n, h] = get\_channel(channel\_type, dims, num\_realizations)}
\emph{Needs to be specified by Nicola}
\end{function}

\begin{function}{s = get_random_symbols(p)}
  Create a sequence with the length of $K_{on}M_{on}$ of random
  symbols in the range of $0\ldots 2^\mu-1$.
\end{function}

\begin{function}{g = get_transmitter_pulse(p)}
  Return $g_{00}$ for the GFDM parameters. $g_{00}$ is the time domain
  impulse response which corresponds to the 0th subsymbol on the 0th
  subcarrier.  In the library (at least) the following pulse shapes
  shall be contained:

  \begin{center}
    \begin{tabular}{rp{12cm}}
      \code{pulse} value & Description \\ \hline
      \code{rrc} & Root raised cosine with rolloff
      \code{a}. Calculated with time-domain equation.  \\
      \code{rc}  & Raised cosine with rolloff \code{a}. Calculated
      with time-domain equation. \\
      \code{xia1st} & 1st order Xia pulse with rolloff
                      \code{a}. Calculated by ifft of frequency
                      domain. \\
      \code{xia4th} & 4th order Xia pulse with rolloff
                      \code{a}. Calculated by ifft of frequency
                      domain. \\
      \code{dirichlet} & Dirichlet kernel, created by ifft of perfect
                         frequency rect. \\
    \end{tabular}
  \end{center}

  \begin{remark}
    It should be easy to add more pulses to the library.  This means
    that there shall be one function for each pulse and the function
    \code{get_transmit_pulse} only delegates to these.
  \end{remark}
\end{function}

\begin{function}{g = get_receiver_pulse(p, recType, snr)}
  Return the receive prototype pulse for the GFDM parameters and the
  given receiver type.  The receive pulse is the first row of the
  receiver matrix $\mat{B}$.
\end{function}

\begin{function}{w = get_window(p)}
Returns the time domain function of the window with that the GFDM
block is multiplied after the CP has been added.
\end{function}

\begin{function}{mset = get_mset(p)}
Create a list of all allocated sub-symbols in the block.  The
following algorithm applies:

\begin{enumerate}
\item If \code{Mset} is present in the parameter structure, return
  \code{Mset}.
\item If \code{Mon} is present in the parameter structure, return
  \code{M-Mon:M-1}, meaning the last \code{Mon} sub-symbols in each block
  are allocated.
\item If neither is given, return \code{0:M-1}, meaning all sub-symbols
  are allocated.
\end{enumerate}
\end{function}

\begin{function}{kset = get_kset(p)}
Create a list of all allocated sub-carriers in the GFDM block.

\begin{enumerate}
\item If \code{Kset} is present in the parameter structure, return
  \code{Kset}.
\item If \code{Kon} is present in the parameter structure, return
  \code{0:K-1}, meaning the first \code{Kon} sub-carriers in each block
  are allocated.
\item If neither is given, return \code{0:K-1}, meaning all sub-symbols
  are allocated.
\end{enumerate}
\end{function}

\section{\code{Calc} Functions}
\begin{function}{ser = calc_ser_awgn(p, recType, snr, channel_response=[1])}
  Calculate the symbol error rate of the GFDM system with the passed
  receiver type.  The channel impulse response sampled at the sampling
  frequency is passed as the last parameter.  It is assumed that the
  CP is long enough to prevent ISI. So far equations for the
  following scenarios are available:
\begin{itemize}
\item SER for matched filter in frequency-selective channel
\item SER for zero-forcing receiver in frequency-selective channel
\end{itemize}

If an unsupported calculation is requested, an error shall be issued.
\end{function}

\begin{function}{ser = calc_ser_rayleigh(p, recType, snr, channel_response=[1])}
Calculate the SER for GFDM in a rayleigh fading channel.  The channel
response shall describe the average fraction of energy  that is
transmitted on each channel tap.  The following scenarios are
currently provided:
\begin{itemize}
\item SER for zero-forcing receiver in frequency selective channel.
\end{itemize}

If an unsupported calculation is requested, an error shall be issued.
\end{function}

\begin{function}{ser = calc_ser_ofdm\_awgn(mu, snr, channel_response=[1], fftLen)}
Calculate the theoretic SER for OFDM in an AWGN frequency-selective channel.
\end{function}

\begin{function}{ser = calc_ser_ofdm_rayleigh(mu, snr, channel_response=[1], fftLen)}
  Calculate the theoretic SER for OFDM in a frequency-selective
  Rayleigh channel.  The channel impulse response describes the
  average fraction of energy that is transmitted on each channel tap.
\end{function}

\begin{function}{P, f = calc_psd(p, numBlocks)}
Calculate the PSD of the transmit signal of the GFDM system.  Respects
\code{Kon, Mon, Kset, Mset}, the cyclic prefix, and the block window
for the calculation of the PSD.  \code{numBlocks} describes the number
of blocks that are concatenated to calculate the PSD.  \code{P} is the
power in each frequency bin, \code{f} are the corresponding
frequencies in units of one subcarrier.
\end{function}

\begin{function}{tau_rms=calc_rms_delay_spread(P, TAU)}
Calculate root mean square delay spread of a given power-delay
profile. \code{P} contains the powers for each channel tap, \code{TAU}
contains the delay for each channel tap.
\end{function}

\section{\code{Do} Functions}
\begin{function}{D = do_map(p, s)}
Map a linear data stream into the GFDM data matrix, respecting the
allocated subcarriers and subsymbols.  The data is split into chunks
of $|K_{set}|$ symbols which are concatened column-wise. In the
resulting matrix zero rows and columns are inserted at the positions,
where no data is allocated to create the final $\mat{D}$.
\end{function}

\begin{function}{s = do_unmap(p, D)}
Reverse the mapping operation of \code{do_map}. Removes the rows and
colums of $\mat{D}$ that correspond to non-allocated sub-carriers and
sub-symbols, afterwards stacks the columns onto each other to produce
the linear data stream.
\end{function}

\begin{function}{x = do_modulate(p, D)}
  Takes a data block \code{D} of dimension \code{KxM} and modulates it
  according to the parameters in \code{p}.  It uses the FFT-based
  version of the transmitter (\cite{Michailow2012d}).  \code{D} can
  contain arbitrary complex values which do not need to lie on the
  constellation points.  This way, also pilots or sync sequences could
  be inserted.  No cyclic prefix is added to the signal.

  \begin{remark}
    There is no choice for the transmitter algorithm (A-matrix of FFT)
    since the result of both is the same but the FFT-based version is
    much faster.
  \end{remark}
\end{function}

\begin{function}{xcp = do_addcp(p, x)}
Take a modulated GFDM signal and add the cyclic prefix according to
the parameters in \code{p}.  Furthermore, multiply the resulting block
with the block window.
\end{function}

\begin{function}{x = do\_removecp(p, xcp)}
  Remove the cyclic prefix of the signal according to the parameters
  in \code{p}.  Depending on the block window, the rolloff-parts are
  added together to restore the correct signal.
\end{function}

\begin{function}{Dhat = do\_demodulate(p, x, recType)}
Demodulate a GFDM signal with different receiver types.  The signal
should not contain a cyclic prefix.  Returns the estimated
soft-symbols of the receiver after the demodulation.  Supported
receiver types are:

\begin{center}
  \begin{tabular}{ll}
    Value & Type \\\hline
    \code{MF} & Matched filter, FFT-based (\cite{Gaspar2013})\\
    \code{ZF} & Zero-forcing, FFT-based (\emph{no reference yet})\\
    \code{MMSE} & Minimum mean square error receiver with matrix \\
    \code{MF-DSIC} & Matched filter receiver with double-sided
    interference cancellation.
  \end{tabular}
\end{center}
\end{function}

\section{Helper Functions}
These functions are not directly related to GFDM but are commonly used
for visualization. For compatibility reasons, the names deviate from
our common naming conventions.

\begin{function}{pgfplot}\end{function}
Export all graphs in the current figure (\code{gcf()}) into txt-files
in the current directory.  See Nicola's Terminology-document for a
more detailed description.
\begin{function}{getFigureDims}\end{function}
\begin{function}{getFigure}\end{function}
\begin{function}{cscatter}\end{function}
\begin{function}{toctoc}\end{function}


\vspace{1cm}
\bibliographystyle{plain}
\bibliography{../../../../../library}

\end{document}
