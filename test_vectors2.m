clear all
close all
load('test_vectors2.mat')

%% Plot the resulting PSD
fint = [103 139]*15e3;
fout = [101 141]*15e3;

figure(2)
hold off

fb = linspace(-fs/2, fs/2, length(sGFDM2)+1); fb = fb(1:end-1)';
b = fftshift(abs(fft(sGFDM2)/sqrt(length(sGFDM2))));
indb=find(fb>=fint(1) & fb<=fint(2));
outb=find(fb<=fout(1) | fb>=fout(2));
Pb = 10*log10(mean(b(indb).^2))
Ob = 10*log10(mean(b(outb).^2))
ACLRb=Pb-Ob
plot(fb, mag2db(b), 'g');
hold on

fc = linspace(-fs/2, fs/2, length(sGFDM)+1); fc = fc(1:end-1)';
c = fftshift(abs(fft(sGFDM)/sqrt(length(sGFDM))));
indc=find(fc>=fint(1) & fc<=fint(2));
outc=find(fc<=fout(1) | fc>=fout(2));
Pc = 10*log10(mean(c(indc).^2))
Oc = 10*log10(mean(c(outc).^2))
ACLRc=Pc-Oc
plot(fc, mag2db(c), 'r');

fa = linspace(-fs/2, fs/2, length(sOFDM)+1); fa = fa(1:end-1)';
a = fftshift(abs(fft(sOFDM)/sqrt(length(sOFDM))));
inda=find(fa>=fint(1) & fa<=fint(2));
outa=find(fa<=fout(1) | fa>=fout(2));
Pa = 10*log10(mean(a(inda).^2))
Oa = 10*log10(mean(a(outa).^2))
ALCRa=Pa-Oa
plot(fa, mag2db(a), 'b');

fd = linspace(-fs/2, fs/2, length(sSCFDMA)+1); fd = fd(1:end-1)';
d = fftshift(abs(fft(sSCFDMA)/sqrt(length(sSCFDMA))));
indd=find(fd>=fint(1) & fd<=fint(2));
outd=find(fd<=fout(1) | fd>=fout(2));
Pd = 10*log10(mean(d(indd).^2))
Od = 10*log10(mean(d(outd).^2))
ACLRd=Pd-Od
plot(fd, mag2db(d), 'y');

fe = linspace(-fs_ufmc/2, fs_ufmc/2, length(sUFMC)+1); fe = fe(1:end-1)';
e = fftshift(abs(fft(sUFMC)/sqrt(length(sUFMC))));
inde=find(fe>=fint(1) & fe<=fint(2));
oute=find(fe<=fout(1) | fe>=fout(2));
Pe = 10*log10(mean(e(inde).^2))
Oe = 10*log10(mean(e(oute).^2))
ACLRe=Pe-Oe
plot(fe, mag2db(e), 'k');


%ylim([-40, 30]);    
xlabel('f [Hz]'); ylabel('PSD [dB]');
xlim([-fs/2,fs/2])
grid()
legend('GFDM','GFDM (1 SF)','OFDM','SC-FDMA','UFMC','Location','NorthWest');
