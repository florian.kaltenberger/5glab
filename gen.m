% Copyright (c) 2014 TU Dresden
% All rights reserved.
% See accompanying license.txt for details.
%

% This example shows how a random GFDM signal can be
% created. Additionally, an OFDM signal with an equal length is
% created, and the Spectrum of both is compared.

addpath('mmatthe-gfdmlib-394322eb8868')
setPath

%% Create parameter sets for GFDM and OFDM
fs = 7.68e6; %sampling rate
fs_ufmc = fs*2; %sampling rate

gfdm = get_defaultGFDM('TTI');
gfdm.K = 512;   %128    
gfdm.Kon = 36;  %75 Only allocate some subcarriers
gfdm.Kset = mod(get_kset(gfdm)+121,gfdm.K);
gfdm.pulse = 'rc';
gfdm.a = 0;
gfdm.M = 14;
gfdm.Mon = gfdm.M-2;
gfdm.Ncp = gfdm.K;
gfdm.B = 1;

gfdm2 = gfdm;
gfdm2.K = 512; %96
gfdm2.Kon = 36;%56
gfdm2.Ncp = gfdm2.K;
gfdm2.B = 20;

ofdm = gfdm;
ofdm.K = 512; %2048
ofdm.Kon = 36;  %1200 Only allocate some subcarriers
ofdm.pulse = 'rc_td';  % use RC_TD with rolloff 0 to make a
ofdm.a = 0;            % rectangular filter
ofdm.M = 1;
ofdm.Mon = 1;
ofdm.B = 6; % Number of OFDM blocks to generate
ofdm.Ncp = round(144/2048*ofdm.K);

ofdm2=ofdm;
ofdm2.B=1;
ofdm2.Ncp = round(160/2048*ofdm2.K);

%% Generate the signals

sGFDM = gen_gfdm(gfdm);
sGFDM2 = gen_gfdm(gfdm2);
sOFDM = [gen_gfdm(ofdm2);gen_gfdm(ofdm);gen_gfdm(ofdm2);gen_gfdm(ofdm)]; %1 subframe

% quantize the signals
sGFDM = round(sGFDM*pow2(12))/pow2(12);
sGFDM2 = round(sGFDM2*pow2(12))/pow2(12);
sOFDM = round(sOFDM*pow2(12))/pow2(12);

SCFDMA
sSCFDMA = txs0(1:7680)./sqrt(10*mean(abs(txs0(1:7680).^2)));

UFMC
sUFMC = txs0(1:7680*2)./sqrt(10*mean(abs(txs0(1:7680*2).^2)));

%% export files for SMBV
addpath('E:\Synchro\kaltenbe\My Documents\openair4G\openair1\SIMULATION\LTE_PHY')
mat2wv(sGFDM, 'GFDM.wv', fs, 1);
mat2wv(sOFDM, 'OFDM.wv', fs, 1);
mat2wv(sSCFDMA, 'SCFDMA.wv', fs, 1);
mat2wv(sUFMC, 'UFMC.wv', fs_ufmc, 1);

save('test_vectors2.mat','sGFDM','sGFDM2','sOFDM','sSCFDMA','sUFMC','fs','fs_ufmc');

